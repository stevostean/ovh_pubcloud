from datetime import datetime

import requests

from proxylist import random_proxy

# SODABAR_URL = "http://soda-bar.fr/hide/loading_page.log" # Before...
# CREATE A NEW URI/ENDPOINT TO GET MORE DETAILS IN HEADERS WITH DATA
SODABAR_URL = "http://soda-bar.fr/hide/logfile/"


class GetLogException(Exception):
    pass


def req_by_proxy(url):
    '''A way to get requests running via proxies
    FROM: http://docs.python-requests.org/en/master/user/advanced/#proxies
    '''
    count = 0
    host1, port1 = random_proxy()
    host2, port2 = random_proxy()
    proxies = (
        {'http': f"http://{host1}:{port1}"},
        {'http': f"http://{host2}:{port2}"},
    )

    try:
        return requests.get(url, stream=True, proxies=proxies, timeout=10.0)
    except requests.exceptions.ProxyError:
        if count < 2:
            count += 1
            req_by_proxy()
        else:
            raise GetLogException("Cannot reach proxies")
    except requests.exceptions.ReadTimeout:
        if count < 2:
            count += 1
            req_by_proxy()
        else:
            raise GetLogException("Too many timeout")
    except requests.exceptions.ConnectTimeout:
        if count < 2:
            count += 1
            req_by_proxy()
        else:
            raise GetLogException("To many attempts to reach proxies")


def get_log(url=SODABAR_URL, download=True, proxy=True):
    '''2 manners to get log file from soda-bar.fr server: content or
    download/write file'''
    if proxy:
        r = req_by_proxy(url)
    else:
        r = requests.get(url, stream=True)

    heads = r.headers

    now = datetime.now()
    # Trying generator instead of list (memory performance)
    # res = [line for line in r.iter_lines()]
    res = r.content

    if download:
        try:
            filename = f"{now.strftime('%d-%m-%Y_%H-%M-%S')}.log"
            with open(filename, "w") as f:
                f.write(res)
            return filename, heads

        except:
            return False

    return res, heads


if __name__ == '__main__':
    get_log()
