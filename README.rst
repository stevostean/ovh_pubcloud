OVH PUBLIC CLOUD PYTHON API
===========================


In case you're running the whole code here, please do:
.. code-block:: bash
    pipenv run python main.py

Then you can check the log in your terminal:
.. code-block:: bash
    tail syncing_ops.log


========================================
PROJECT IS RUNNING ON A DEDICATED SERVER
========================================

This **Python Module** is actually running on dedicated server (Kimsufi, with 4-cores) to check any updates, at any value around 20 minutes (randomly). So it gets any changes from the client's webserver and the OVH Public Cloud instance, via MD5 checksums.

**If any change is true:**  it gets the log content from the client's webserver and then re-write it on the OVH Public Cloud instance.

All this work to switch from AWS S3 expensive data transfer costs to a - maybe - cheap solution, made possible by OVH. Let's do the job for a while and then, I'll came back with an update, right?

NOTES:
    **proxylist:** this script was made ONLY to request URI with any kind of proxy servers, randomly selected...
