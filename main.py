from checksum import (
    check_content,
    # check_file,
)
from ovh import OvhPublicCloud
from server_log import get_log
from utils.logger import Log, STREAM_HANDLER


cloud = OvhPublicCloud()


def cloud_task():
    task = "CLOUD"
    log_in_cloud_name = cloud.get_last_object_name() or None

    if log_in_cloud_name is not None:
        Log.info(f"{task}: cloud filename={log_in_cloud_name}")

        log_in_cloud = cloud.get_object_content(log_in_cloud_name)

        checksum_key = "x-object-meta-checksum"
        log_in_cloud_checksum = cloud.get_object_headers(log_in_cloud_name,
                                                         key=checksum_key)

        if log_in_cloud_checksum:
            log_checksum = int(log_in_cloud_checksum)
        else:
            log_checksum = check_content(log_in_cloud,
                                         headers=True,
                                         hexa=False)
        Log.info(f"{task}: cloud file checksum={log_checksum}")
    else:
        log_checksum = 0

    return log_checksum, log_in_cloud_name


def webserser_task():
    task = "WEBSERVER"
    log_in_webserver = get_log(download=False, proxy=False)
    log_in_webserver_content, log_in_webserver_headers = log_in_webserver

    serverlog_checksum = None
    special_header = "X-Object-Meta-md5_checksum"

    if special_header in log_in_webserver[1].keys():
        tmp_checksum = log_in_webserver_headers.get(special_header, None)
        serverlog_checksum = int(tmp_checksum, 16)

    weight = log_in_webserver_headers.get('Content-Length', 0)
    Log.info(f"{task}: size of web log content={weight} bytes")

    if serverlog_checksum is None:
        serverlog_checksum = check_content(log_in_webserver[0],
                                           headers=False,
                                           hexa=False)
    Log.info(f"{task}: web log content checksum={serverlog_checksum}")

    return serverlog_checksum, log_in_webserver_content


def main_task(stream=True):
    if not stream:
        Log.removeHandler(STREAM_HANDLER)
    task = "MAIN"
    ovh_checksum, ovh_filename = cloud_task()
    ws_checksum, ws_content = webserser_task()

    # code = 0
    if ovh_checksum == ws_checksum:
        Log.warning(f"{task}: both checksums are identical")
    else:
        Log.info("Updating cloud log file")
        if ovh_checksum != 0:
            Log.info(f"{task}: deleting old file={ovh_filename}")
            cloud.delete(ovh_filename)

        Log.info(f"{task}: uploading content to cloud={ws_checksum}")
        metadata = {'X-Object-Meta-Checksum': ws_checksum}
        ovh_new_filename = cloud.upload_content(ws_content,
                                                name="sodata.log",
                                                meta=metadata)

        Log.info(f"{task}: new log file on cloud={ovh_new_filename}")

    # exit(code)
    return


if __name__ == '__main__':
    main_task()
