OS_PASSWORD = ""

OS_AUTH_URL = ""

# With the addition of Keystone we have standardized on the term **tenant**
# as the entity that owns the resources.
OS_TENANT_ID = ""
OS_TENANT_NAME = ""

# In addition to the owning entity (tenant), openstack stores the entity
# performing the action as the **user**.
OS_USERNAME = ""

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments. Like
# "SBG3" for Strasbourg.
OS_REGION_NAME = "SBG3"
