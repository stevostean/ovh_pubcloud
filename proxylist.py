import csv
from datetime import datetime
from glob import glob
from os import remove
from random import shuffle

from lxml import html as ht
import requests


URL = "https://free-proxy-list.net/anonymous-proxy.html"


def get_proxies():
    res = requests.get(URL)
    content = res.content
    html = ht.document_fromstring(content)

    rows = html.xpath('//table/tbody/tr')

    proxies = []

    for elem in rows:
        ip = elem.xpath('td/text()')[0]
        port = elem.xpath('td/text()')[1]

        proxies.append((ip, port))

    return proxies


def write_file(proxylist):
    file_list = glob('./*.csv')
    if len(file_list) > 0:
        for item in file_list:
            remove(item)
    nowaday = today()
    try:
        with open(f"{nowaday}.csv", "w", newline="") as f:
            csvfile = csv.writer(f, delimiter=',',
                                 quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for item in proxylist:
                csvfile.writerow([item[0], item[1]])
        return True
    except:
        raise IOError("Cannot write csv file")


def today():
    now = datetime.utcnow()
    return now.strftime("%Y%m%d-%H%M%S")


def random_proxy():
    csvfiles = glob('*.csv')
    proxies = []
    if len(csvfiles) > 0:
        with open(csvfiles[-1], newline='') as f:
            csvcontent = csv.reader(f, delimiter=',', quotechar='|')
            for elem in csvcontent:
                proxies.append((elem[0], elem[1]))
        shuffle(proxies)
        return proxies[0]

    proxies.append(get_proxies())
    shuffle(proxies)
    return proxies[0]


if __name__ == '__main__':
    long_sentence = "Write a CSV file containing the last free proxies."
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file",
                        help=long_sentence,
                        action="store_true")
    parser.add_argument("-r", '--random',
                        help="return random proxy server.",
                        action="store_true")

    args = parser.parse_args()
    if args.file:
        write_file(get_proxies())
    if args.random:
        print(random_proxy())
