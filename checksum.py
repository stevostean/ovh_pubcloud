import os

from hashlib import md5 as hasher


def check_content(content, hexa=True, headers=True):
    '''get checksum from content
    :content any
    :hexa bool (hexa string | integer string)
    :headers bool (?)
    '''
    content = content[1].strip() if headers else content.strip()
    if hexa:
        return hasher(content).hexdigest()
    return int(hasher(content).hexdigest(), 16)


def check_file(file_name, hexa=True):
    '''get checksum from file content
    :hexa bool (hexa string | integer string)
    '''
    if not os.path.exists(file_name):
        raise FileNotFoundError(f"File {file_name} not found.")

    with open(file_name, "rb") as f:
        contents = f.read()

    if hexa:
        return hasher(contents).hexdigest()
    return int(hasher(contents).hexdigest(), 16)
