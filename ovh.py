from datetime import datetime
from inspect import getmembers, ismethod
import sys

from swiftclient import Connection, ClientException

from ovh_credentials import (
    OS_AUTH_URL,
    OS_PASSWORD,
    # OS_REGION_NAME,
    # OS_TENANT_ID,
    OS_TENANT_NAME,
    OS_USERNAME,
    TARGET_CONTAINER,
)

from utils.classes import singleton

conn = Connection(
    user=OS_USERNAME,
    key=OS_PASSWORD,
    authurl=OS_AUTH_URL,
    tenant_name=OS_TENANT_NAME,
    auth_version="2.0"
)

main = conn.get_account()[1]


class OvhPublicCloudException(Exception):
    pass


@singleton
class OvhPublicCloud:
    _buffer = None

    def __init__(self):
        self._conn = Connection(
            user=OS_USERNAME,
            key=OS_PASSWORD,
            authurl=OS_AUTH_URL,
            tenant_name=OS_TENANT_NAME,
            auth_version="2.0"
        )
        self._account = self._conn.get_account()[1]
        self._container = self._conn.get_container(TARGET_CONTAINER)
        self._objects = self._container[1]

    def __getitem__(self, index):
        '''make this class subscriptable (OvhPublicCloud['objects'])'''
        return getattr(self, index)

    @property
    def objects(self):
        return self._objects

    @property
    def objects_name(self):
        return tuple(item['name'] for item in self.objects)

    @property
    def account(self):
        return self._account

    def get_last_object_name(self):
        try:
            return self.objects_name[-1]
        except:
            return None

    def nblm(self):
        '''list objects, ordered by tuple
        like n: name, b: bytes, lm: last_modified'''
        return [(item['name'], item['bytes'], item['last_modified'])
                for item in self.objects]

    def file_exists(self, filename):
        for obj in self._objects:
            if filename in obj.values():
                return obj

        return False

    def get_object_headers(self, obj_name, key=False):
        _headers = self._conn.head_object(TARGET_CONTAINER, obj_name)
        if _headers:
            if key:
                if key in _headers.keys():
                    return _headers[key]
                else:
                    return False

            return _headers
        return False

    def get_object_content(self, object_name):
        target = self.file_exists(object_name)
        if target:
            return self._conn.get_object(TARGET_CONTAINER, object_name)
        return None

    def download(self, object_name, filename=None):
        obj_content = self.get_object_content(object_name)
        if obj_content is not None:
            try:
                with open(filename, "wb") as f:
                    f.write(obj_content)
                return filename
            except:
                raise OvhPublicCloudException(f"{filename} was not created")
        return None

    def delete(self, filename):
        target = self.file_exists(filename)
        if target:
            try:
                self._conn.delete_object(TARGET_CONTAINER, filename)
                return True
            except ClientException as e:
                string = f"Failed to delete object in cloud: {e}"
                raise OvhPublicCloudException(string)

    def upload(self, filename=None, meta={}):
        try:
            with open(filename, "rb") as f:
                self._conn.put_object(
                    TARGET_CONTAINER,
                    filename,
                    contents=f.read(),
                    content_type="application/octet-stream",
                    headers=meta
                )
            return True
        except:
            string = f"Failed to upload {filename}"
            raise OvhPublicCloudException(string)

    def upload_content(self, content, name=None, meta={}):
        if name is None:
            name = datetime.utcnow()
            name = name.strftime("%Y%m%d_%H-%M-%S")
            name = f"{name}.log"

        try:
            self._conn.put_object(
                TARGET_CONTAINER,
                name,
                contents=content,
                content_type="application/octet-stream",
                headers=meta
            )
            return name
        except:
            string = "failed to upload content to the cloud"
            raise OvhPublicCloudException(string)


if __name__ == '__main__':
    o = OvhPublicCloud()
    try:
        while True:
            methods = getmembers(o, predicate=ismethod)
            properties = ", ".join(o.__dict__.keys())
            SEPARATORS = "-" * 30
            print(SEPARATORS)
            print("METHODS")
            print(", ".join([attr for attr in dir(o)]))
            print(SEPARATORS)
            print("PROPERTIES")
            print(properties)
            print(SEPARATORS)
            req = input("Method/Property ~> ")
            try:
                meth, *args = req.split(' ')
                if len(args) < 1:
                    try:
                        print(getattr(o, meth)())
                    except TypeError:
                        print(o[meth])
                else:
                    arguments = ", ".join([arg for arg in args])
                    print(arguments)
                    print(getattr(o, meth)(arguments))

            except AttributeError:
                print("Method/Property unavailable")
    except KeyboardInterrupt:
        sys.exit(0)
