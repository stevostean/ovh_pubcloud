from sys import exit

from main import main_task
from utils.timer import (
    # schedule_function,
    Scheduler,
)


if __name__ == '__main__':
    try:
        # schedule_function(main_task, interval=10, args=(False,))
        t = Scheduler(main_task, False)
        t.around(20, step=0.2).minutes()
        t.start()
    except KeyboardInterrupt:
        t.stop()
        exit(0)
