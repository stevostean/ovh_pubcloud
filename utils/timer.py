from random import uniform as randfloat
from threading import Timer
from time import sleep

from utils.logger import Log, STREAM_HANDLER


def schedule_function(function, interval=1, args=()):
    """scheduling a function every interval, with args
        :function callback function (without parenthesis)
        :interval int|float (in seconds)
        :args tuple (arguments for the callback function)
    """
    delay = float(interval)
    function(*args)
    Timer(interval, schedule_function, (function, delay, args)).start()
    sleep(1)


class Scheduler:
    """scheduling task with a callback function & its
    arguments / keyword arguments avaery interval

    Attributes:
        args (LIST): list of arguments for callback function
        kwargs (DICT): dictionnary of keyword arguments for callback function
        thread (threading.TIMER): instance of threading.timer to self
        execute this instance
    """

    def __init__(self, func, *args, **kwargs):
        self._delay = 1
        self._func = func
        self.args = args
        self.kwargs = kwargs
        self.thread = None
        Log.removeHandler(STREAM_HANDLER)

    def start(self):
        """runs all operations involved in this instance
        """
        if self.thread is not None:
            self.thread.join()
        self.callback()

    def stop(self):
        """cancel all operations in this instance
        """
        self._delay = 0
        self.thread.cancel()

    def callback(self):
        """auto execute the callback function with args & kwargs
        + recursion on threading.timer object
        """
        if self._delay > 0:
            Log.debug(f"SCHEDULER: check every {self._delay} sec")
            self._func(*self.args, **self.kwargs)
            self.thread = Timer(self._delay, self.callback, ).start()
        else:
            self.stop()

    def every(self, value=1):
        """set the delay of each instance of threading.timer

        Args:
            value (int|float): any value to set a time

        Returns:
            TYPE: this instance
        """
        if value != 1:
            self._delay = value
        return self

    def around(self, value=1, step=0.5):
        """pick a random floating value around the value specified.
        A step is also available.

        Args:
            value (int, optional): the number around the random value
            is generated
            step (float, optional): fine-tuning the range of floating values

        Returns:
            TYPE: this instance
        """
        entry_val = float(value)
        entry_step = float(step) if step > 0 else 0.01
        min_entry = entry_val * (1 - entry_step)
        max_entry = entry_val * (1 + entry_step)
        r_range = randfloat(min_entry, max_entry)
        if r_range > 1.0:
            self._delay = r_range
        return self

    def seconds(self):
        """set the delay to seconds, as expected

        Returns:
            TYPE: this instance
        """
        return self

    def minutes(self):
        """converts the delay value into minutes (X60 seconds)

        Returns:
            TYPE: this instance
        """
        self._delay = self._delay * 60
        return self

    def hours(self):
        """converts the delay value into hours(X60 self.minutes)

        Returns:
            TYPE: this instance
        """
        self._delay = self.minutes() * 60
        return self

    def days(self):
        """converts the delay value into days (X24 this.hours)
        """
        self._delay = self.hours() * 24
        return self
