class SingletonWrapper:
    """A class to wrap any other class and make it singleton"""

    def __init__(self, klass):
        self._klass = klass
        self._instance = None

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = self._klass(*args, **kwargs)

        return self._instance


def singleton(cls):
    '''A simple singleton decorator for classes
    FROM: http://elbenshira.com/blog/singleton-pattern-in-python/
    '''
    instances = {}

    def getinstances():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstances


def isprop(v):
    '''functional method to get all properties from instance's class'''
    return isinstance(v, property)
