from datetime import datetime as dt
import logging

from logging.handlers import RotatingFileHandler

LOGGER_NAME = "sodabar"


class MsFormatter(logging.Formatter):

    """adding milliseconds feature into timestamp logging format

    FROM: https://stackoverflow.com/questions/6290739/python-logging-use-milliseconds-in-time-format/#answer-8911107

    Attributes:
        converter (datetime.datetime): datetime timestamp
    """

    converter = dt.fromtimestamp

    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y%m%d-%H%M%S")
            s = "%s.%03d" % (t, record.msecs)
        return s


logging.getLogger("requests").setLevel(logging.WARNING)
Log = logging.getLogger(LOGGER_NAME)
Log.setLevel(logging.DEBUG)

formatter = MsFormatter(fmt="%(asctime)s :: %(levelname)s :: %(message)s")

# LOG FILE: 100 backups of 1 Mb size.
FILE_HANDLER = RotatingFileHandler("syncing_ops.log", "a", 1000000, 100)
FILE_HANDLER.setFormatter(formatter)
Log.addHandler(FILE_HANDLER)

STREAM_HANDLER = logging.StreamHandler()
STREAM_HANDLER.setFormatter(formatter)
Log.addHandler(STREAM_HANDLER)
